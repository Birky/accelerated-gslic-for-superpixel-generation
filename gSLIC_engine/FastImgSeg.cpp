#include "FastImgSeg.h"
#include "cudaSegEngine.h"

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <fstream>
#include <time.h>
#include <iostream>
#include <fstream>

#include "cudaDefines.h"

#include <opencv2/core/core.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/imgproc/imgproc.hpp>
#include "opencv\cv.h"
#include "opencv\highgui.h"
#include <opencv2/gpu/gpu.hpp>

using namespace std;

int ECV = 1;

FastImgSeg::FastImgSeg()
{

}

FastImgSeg::~FastImgSeg()
{
	clearFastSeg();
}


void FastImgSeg::changeClusterNum(int nSegments)
{
	nSeg=nSegments;
}

void FastImgSeg::initializeFastSeg(int w,int h, int nSegments, int nIterations)
{
	width=w;
	height=h;
	nSeg=nSegments;
	SPsize = w*h/nSegments;
	time = 0;
	cTime = 0;

	segMask=(int*) malloc(width*height*sizeof(int));
	pixelsOfSP = (int*)malloc(sizeof(int)*nSeg*SEGMULT*SPsize*SPXMULT);
	markedImg=(unsigned char*)malloc(width*height*4*sizeof(unsigned char));

	InitCUDA(width,height,nSegments,nIterations);

	bImgLoaded=false;
	bSegmented=false;
	bMeanValueCalculated=false;
}

void FastImgSeg::clearFastSeg()
{
	free(segMask);
	free(markedImg);
	free(pixelsOfSP);
	TerminateCUDA();
	bImgLoaded=false;
	bSegmented=false;
	bMeanValueCalculated=false;
}


void FastImgSeg::LoadImg(unsigned char* imgP)
{
	sourceImage=imgP;
	CUDALoadImg(sourceImage);
	bSegmented=false;
}

void FastImgSeg::DoSegmentation(int eMethod, double weight)
{
		clock_t start,finish;

	    start=clock();
		CudaSegmentation(nSeg,eMethod, weight);
		finish=clock();
		printf("clustering: %.0f ms\t",(double)(finish-start));

		CopyMaskDeviceToHost(segMask,width,height);

		int nnSeg = nSeg; // The enforce connectivity function change the number of segments
		start=clock();
		if(ECV == 1)
			enforceConnectivityNative(segMask,width,height,nSeg);
		if(ECV == 2)
			enforceConnectivityModified(segMask,width,height,nnSeg, pixelsOfSP);
		if(ECV == 3)
		{
			cv::Mat klabelsMat;
			cv::Mat klabelsMat32(height, width, CV_32SC1, segMask);
			klabelsMat32.convertTo(klabelsMat, CV_16UC1);
			int SizeStrElem3 = 3;
			cv::Mat  strElem3 = cv::getStructuringElement(cv::MORPH_ELLIPSE, cv::Size(SizeStrElem3, SizeStrElem3));
			cv::erode(klabelsMat, klabelsMat, strElem3);
			cv::dilate(klabelsMat, klabelsMat, strElem3);
			cv::dilate(klabelsMat, klabelsMat, strElem3);
			cv::erode(klabelsMat, klabelsMat, strElem3);

			int SizeStrElem5 = 5;
			cv::Mat  strElem5 = cv::getStructuringElement(cv::MORPH_ELLIPSE, cv::Size(SizeStrElem5, SizeStrElem5));
			cv::erode(klabelsMat, klabelsMat, strElem5);
			cv::dilate(klabelsMat, klabelsMat, strElem5);
			cv::dilate(klabelsMat, klabelsMat, strElem5);
			cv::erode(klabelsMat, klabelsMat, strElem5);

			int SizeStrElem7 = 7;
			cv::Mat  strElem7 = cv::getStructuringElement(cv::MORPH_ELLIPSE, cv::Size(SizeStrElem7, SizeStrElem7));
			cv::erode(klabelsMat, klabelsMat, strElem7);
			cv::dilate(klabelsMat, klabelsMat, strElem7);
			cv::dilate(klabelsMat, klabelsMat, strElem7);
			cv::erode(klabelsMat, klabelsMat, strElem7);

			cv::Mat klabelsMat2(klabelsMat);
			klabelsMat.convertTo(klabelsMat32, CV_32SC1);
			memcpy((char*)segMask, (char*)klabelsMat32.data, height*width*4 );
		}		
		finish=clock();
		time += (float)(finish-start);
		cTime++;
		printf("connectivity: %.0f ms\n",(double)(time/(float)cTime));

		// if you want the mean value of SPs uncomment the following 5 lines. Only RGB is supported yet with displaying, if you want other color space mean value to display, it is needed to convert it back to RGB
		/*CopyMaskHostToDevice(segMask,pixelsOfSP,width,height);
		CudaFeatures(nnSeg,SPsize*SPXMULT);
		hMeanValues = (float3*)malloc(sizeof(float3)*nnSeg);
		CopyMeanValuesDeviceToHost(hMeanValues,nnSeg);
		bMeanValueCalculated = true;*/

		bSegmented=true;
}

void FastImgSeg::Tool_GetMarkedImg(int x, int y)
{
	if (!bSegmented)
	{
		return;
	}

	memcpy(markedImg,sourceImage,width*height*4*sizeof(unsigned char));
	
	int spID = -1;
	if(x != -1 || y != -1)
	{
		spID = segMask[y*width + x];
		printf("Superpixel number: %d\n",spID);
	}
	
	for (int i=1;i<height-1;i++)
	{
		for (int j=1;j<width-1;j++)
		{
			int mskIndex=i*width+j;
			 if(segMask[mskIndex]!=segMask[mskIndex+1] 
			|| segMask[mskIndex]!=segMask[(i-1)*width+j]
			|| segMask[mskIndex]!=segMask[mskIndex-1]
			|| segMask[mskIndex]!=segMask[(i+1)*width+j])
			{
				markedImg[mskIndex*4]=0;
				markedImg[mskIndex*4+1]=0;
				markedImg[mskIndex*4+2]=255;
			}
			
			// if the mean values of superpixels are calculated
			if(bMeanValueCalculated)
			{
				markedImg[mskIndex*4]=hMeanValues[segMask[mskIndex]].x;
				markedImg[mskIndex*4+1]=hMeanValues[segMask[mskIndex]].y;
				markedImg[mskIndex*4+2]=hMeanValues[segMask[mskIndex]].z;
			}

			if(segMask[mskIndex] == spID)
			{
				markedImg[mskIndex*4]=0;
				markedImg[mskIndex*4+1]=255;
				markedImg[mskIndex*4+2]=0;
			}	
		}
	}
}

// from orginal gSLIC - unchanged
void FastImgSeg::Tool_WriteMask2File(char* outFileName, bool writeBinary)
{
	if (!bSegmented)
	{
		return;
	}
	
	if (writeBinary)
	{
		ofstream outf;
		outf.open(outFileName, ios::binary);

		outf.write(reinterpret_cast<char*>(&width),sizeof(width));
		outf.write(reinterpret_cast<char*>(&height),sizeof(height));

		for (int i=0;i<height;i++)
		{
			for (int j=0;j<width;j++)
			{
				int mskIndex=i*width+j;
				int idx=segMask[mskIndex];
				outf.write(reinterpret_cast<char*>(&idx),sizeof(idx));
			}
		}
		outf.close();
	}
	else
	{
		ofstream outf;

		outf.open(outFileName);

		for (int i=0;i<height;i++)
		{
			for (int j=0;j<width;j++)
			{
				int mskIndex=i*width+j;
				int idx=segMask[mskIndex];
				outf<<idx<<' ';
			}
			outf<<'\n';
		}
		outf.close();
	}
}