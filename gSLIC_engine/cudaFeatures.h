#ifndef __CUDA_FEATURES__
#define __CUDA_FEATURES__

#include "cuda.h"
#include "cuda_runtime.h"
#include "device_launch_parameters.h"
#include "cudaDefines.h"

__host__ void calculateMeanValue(float4* floatBuffer, int* pixelsOfSP, int nnSeg, int maxSPsize, float3* meanValues);
__global__ void kCalculateMeanValue(float4* floatBuffer, int* pixelsOfSP, int maxSPsize, float3* meanValues);

#endif