#include "cudaFeatures.h"

__host__ void calculateMeanValue(float4* floatBuffer, int* pixelsOfSP, int nnSeg, int maxSPsize, float3* meanValues)
{
	dim3 ThreadPerBlock(BLOCK_SIZE1);
	dim3 BlockPerGrid(nnSeg);
	kCalculateMeanValue<<<BlockPerGrid,ThreadPerBlock>>>(floatBuffer,pixelsOfSP,maxSPsize, meanValues);
}

__global__ void kCalculateMeanValue(float4* floatBuffer, int* pixelsOfSP, int maxSPsize, float3* meanValues)
{
	int bOffset = blockIdx.x * maxSPsize;
	__shared__ short SPsize; SPsize = pixelsOfSP[bOffset]; // number of pixels in the current SP
	__shared__ float3 meanValue[BLOCK_SIZE1];

	int pOffset = threadIdx.x+1; // the first element is the number of pixels
	int tOffset = threadIdx.x;
	float3 lMeanValue = {0};
	while(pOffset<=SPsize)
	{
		lMeanValue.x += floatBuffer[pixelsOfSP[bOffset+pOffset]].x;
		lMeanValue.y += floatBuffer[pixelsOfSP[bOffset+pOffset]].y;
		lMeanValue.z += floatBuffer[pixelsOfSP[bOffset+pOffset]].z;
		pOffset += blockDim.x;
	}
	meanValue[tOffset].x = lMeanValue.x;
	meanValue[tOffset].y = lMeanValue.y;
	meanValue[tOffset].z = lMeanValue.z;
	__syncthreads();

	#pragma unroll
	for (unsigned int s=BLOCK_SIZE1>>1; s>0; s>>=1) 
	{
		if (threadIdx.x < s)
		{
			meanValue[tOffset].x += meanValue[tOffset + s].x;
			meanValue[tOffset].y += meanValue[tOffset + s].y;
			meanValue[tOffset].z += meanValue[tOffset + s].z;	
		}
		__syncthreads();
	}
	// Unrolling the last warp manually does not improve the speed of the algorithm
	// you can test it by uncomment the following code and set the previous for cycle condition to: s>32
	/*if(threadIdx.x < 32)
	{
		volatile __shared__ float3* vMeanValue;
		vMeanValue = meanValue;

		vMeanValue[tOffset].x += vMeanValue[tOffset + 32].x;
		vMeanValue[tOffset].y += vMeanValue[tOffset + 32].y;
		vMeanValue[tOffset].z += vMeanValue[tOffset + 32].z;

		vMeanValue[tOffset].x += vMeanValue[tOffset + 16].x;
		vMeanValue[tOffset].y += vMeanValue[tOffset + 16].y;
		vMeanValue[tOffset].z += vMeanValue[tOffset + 16].z;

		vMeanValue[tOffset].x += vMeanValue[tOffset + 8].x;
		vMeanValue[tOffset].y += vMeanValue[tOffset + 8].y;
		vMeanValue[tOffset].z += vMeanValue[tOffset + 8].z;

		vMeanValue[tOffset].x += vMeanValue[tOffset + 4].x;
		vMeanValue[tOffset].y += vMeanValue[tOffset + 4].y;
		vMeanValue[tOffset].z += vMeanValue[tOffset + 4].z;

		vMeanValue[tOffset].x += vMeanValue[tOffset + 2].x;
		vMeanValue[tOffset].y += vMeanValue[tOffset + 2].y;
		vMeanValue[tOffset].z += vMeanValue[tOffset + 2].z;

		vMeanValue[tOffset].x += vMeanValue[tOffset + 1].x;
		vMeanValue[tOffset].y += vMeanValue[tOffset + 1].y;
		vMeanValue[tOffset].z += vMeanValue[tOffset + 1].z;
	}
	*/
	if(threadIdx.x == 0)
	{
		meanValues[blockIdx.x].x = meanValue[0].x / SPsize;
		meanValues[blockIdx.x].y = meanValue[0].y / SPsize;
		meanValues[blockIdx.x].z = meanValue[0].z / SPsize;
	}
}