#include "cudaSegEngine.h"
#include "cudaUtil.h"
#include "cudaImgTrans.h"
#include "cudaSegSLIC.h"
#include "cudaImgTrans.h"
#include "cudaFeatures.h"

#include <stdio.h>
#include <math.h>
#include <time.h>
#include <iostream>

using namespace std;

__device__ uchar4* rgbBuffer;
__device__ float4* floatBuffer;
__device__ int* maskBuffer;
__device__ int* dPixelsOfSP;
__device__ float3* dMeanValues;

int nWidth,nHeight,nSeg,nMaxSegs, nIt;
bool cudaIsInitialized=false;

// for SLIC segmentation
int nClusterSize;
int nClustersPerCol;
int nClustersPerRow;
int nBlocksPerCluster;
int nBlocks;

int nBlockWidth;
int nBlockHeight;

__device__ SLICClusterCenter* vSLICCenterList;
bool slicIsInitialized=false;

__host__ void InitCUDA(int width, int height,int nSegment, int nIterations)
{
	//for all methods
	if (!cudaIsInitialized)
	{
		nWidth=width;
		nHeight=height;
		nIt = nIterations;

		cudaMalloc((void**) &rgbBuffer,width*height*sizeof(uchar4));
		cudaMalloc((void**) &floatBuffer,width*height*sizeof(float4));
		cudaMalloc((void**) &maskBuffer,width*height*sizeof(int));
		cudaMalloc((void**) &dPixelsOfSP,SEGMULT*width*height*SPXMULT*sizeof(int));// the size of the superpixels are different, we are allocating enough space for each SPs, we are avoiding dynamic allocation for each SP seperately because of efficiency

		cudaMemset(floatBuffer,0,width*height*sizeof(float4));
		cudaMemset(maskBuffer,0,width*height*sizeof(int));

		nSeg=nSegment;
		cudaIsInitialized=true;
	}


	if (!slicIsInitialized)
	{
		nClusterSize=(int)sqrt((float)iDivUp(nWidth*nHeight,nSeg));

		nClustersPerCol=iDivUp(nHeight,nClusterSize);
		nClustersPerRow=iDivUp(nWidth,nClusterSize);
		nBlocksPerCluster=iDivUp(nClusterSize*nClusterSize,BLOCK_SIZE1);
		nSeg=nClustersPerCol*nClustersPerRow;
		nMaxSegs=iDivUp(nWidth,BLOCK_SIZE2)*iDivUp(nHeight,BLOCK_SIZE2);
		nBlocks=nSeg*nBlocksPerCluster;

		nBlockWidth=nClusterSize;
		nBlockHeight=iDivUp(nClusterSize,nBlocksPerCluster);

		// the actual number of segments
		cudaMalloc((void**) &vSLICCenterList,nMaxSegs*sizeof(SLICClusterCenter));
		cudaMemset(vSLICCenterList,0,nMaxSegs*sizeof(SLICClusterCenter));
		slicIsInitialized=true;
	}

}

extern "C" __host__ void CUDALoadImg(unsigned char* imgPixels)
{
	if (cudaIsInitialized)
	{
		cudaMemcpy(rgbBuffer,imgPixels,nWidth*nHeight*sizeof(uchar4),cudaMemcpyHostToDevice);
	}
	else
	{
		return;
	}
}

__host__ void TerminateCUDA()
{
	if (cudaIsInitialized)
	{
		cudaFree(rgbBuffer);
		cudaFree(floatBuffer);
		cudaFree(maskBuffer);
		cudaFree(dPixelsOfSP);
		cudaFree(dMeanValues);
		cudaIsInitialized=false;
	}

	if (slicIsInitialized)
	{
		cudaFree(vSLICCenterList);
		slicIsInitialized=false;
	}

}

__host__ void CudaSegmentation( int nSegments, int eSegmethod, double weight)
{
	nSeg=nSegments;

	switch (eSegmethod)
	{
	case 1 :

		Rgb2CIELab(rgbBuffer,floatBuffer,nWidth,nHeight);
		SLICImgSeg(maskBuffer,floatBuffer,nWidth,nHeight,nSeg,nIt,vSLICCenterList,(float)weight);

		break;

	case 3:

		Uchar4ToFloat4(rgbBuffer,floatBuffer,nWidth,nHeight);
		SLICImgSeg(maskBuffer,floatBuffer,nWidth,nHeight,nSeg,nIt,vSLICCenterList,(float)weight);

		break;

	case 2:

		Rgb2XYZ(rgbBuffer,floatBuffer,nWidth,nHeight);
		SLICImgSeg(maskBuffer,floatBuffer,nWidth,nHeight,nSeg,nIt,vSLICCenterList,(float)weight);

		break;	
	}
	cudaThreadSynchronize();
}

__host__ void CopyMaskDeviceToHost( int* maskPixels, int width, int height)
{
	if (cudaIsInitialized)
	{
		cudaMemcpy(maskPixels,maskBuffer,nHeight*nWidth*sizeof(int),cudaMemcpyDeviceToHost);
	}
}

__host__ void CopyMaskHostToDevice( int* maskPixels, int* pixelsOfSP, int width, int height)
{
	cudaMemcpy(maskBuffer,maskPixels,nHeight*nWidth*sizeof(int), cudaMemcpyHostToDevice);
	cudaMemcpy(dPixelsOfSP,pixelsOfSP,sizeof(int)*SEGMULT*width*height*SPXMULT,cudaMemcpyHostToDevice);
}

__host__ void CudaFeatures(int nSeg, int maxSPsize)
{
	// Uchar4ToFloat4 is converting uchar RGB to float RGB, if you want other color space you have to use another conversion function, for example Rgb2CIELab
	Uchar4ToFloat4(rgbBuffer,floatBuffer,nWidth,nHeight);
	cudaMalloc((void**) &dMeanValues,nSeg*sizeof(float3));
	calculateMeanValue(floatBuffer,dPixelsOfSP,nSeg,maxSPsize,dMeanValues);
}

 __host__ void CopyMeanValuesDeviceToHost(float3* hMeanValues,int nSeg)
 {
	 cudaMemcpy(hMeanValues,dMeanValues,nSeg*sizeof(float3),cudaMemcpyDeviceToHost);
 }