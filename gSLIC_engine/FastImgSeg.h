#ifndef __CUDA_SUPERPIXELSEG__
#define __CUDA_SUPERPIXELSEG__

#include "cudaUtil.h"
#include "cudaSegSLIC.h"



class FastImgSeg
{

public:
	unsigned char* sourceImage;
	unsigned char* markedImg;
	int* segMask;
	int* pixelsOfSP;
	float3* hMeanValues;

private:

	int width;
	int height;
	int nSeg;
	int SPsize;
	float time;
	int cTime;

	bool bSegmented;
	bool bImgLoaded;
	bool bMaskGot;
	bool bMeanValueCalculated;
	int nIt;

public:
	FastImgSeg();
	~FastImgSeg();

	void initializeFastSeg(int width,int height,int nSegments, int nIterations);
	void clearFastSeg();
	void changeClusterNum(int nSegments);

	void LoadImg(unsigned char* imgP);
	void DoSegmentation(int eMethod, double weight);
	void Tool_GetMarkedImg(int x, int y);
	void Tool_WriteMask2File(char* outFileName, bool writeBinary);
};

#endif
