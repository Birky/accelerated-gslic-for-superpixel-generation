#ifndef __CUDA_SEG_SLIC__
#define  __CUDA_SEG_SLIC__

#include "cuda.h"
#include "cuda_runtime.h"
#include "device_launch_parameters.h"


typedef struct
{
	float4 lab;
	float2 xy;
	int nPoints;

}SLICClusterCenter;

__host__ void SLICImgSeg(int* maskBuffer, float4* floatBuffer, 
						 int nWidth, int nHeight, int nSegs, int nIt,
						 SLICClusterCenter* vSLICCenterList, 
						 float weight);

__global__ void kInitClusterCentersNative(float4* floatBuffer, 
									int nWidth, int nHeight, int nSegs,  
									SLICClusterCenter* vSLICCenterList);

__global__ void kInitClusterCentersImproved(const float4* __restrict__ floatBuffer, int nWidth, int nHeight, int nSegs, SLICClusterCenter* vSLICCenterList );


__global__ void kIterateKmeansNative(int* maskBuffer, float4* floatBuffer, 
							   int nWidth, int nHeight, int nSegs, int nClusterIdxStride, 
							   SLICClusterCenter* vSLICCenterList, 
							   bool bLabelImg, float weight);

__global__ void kIterateKmeansImproved( int* maskBuffer,const float4* __restrict__ floatBuffer, 
							   int nWidth, int nHeight, int nSegs,  int nClusterIdxStride, 
							   const SLICClusterCenter* __restrict__ vSLICCenterList, 
							   bool bLabelImg, float weight);

__global__ void kUpdateClusterCentersNative(float4* floatBuffer, int* maskBuffer,
										  int nWidth, int nHeight, int nSegs,  
										  SLICClusterCenter* vSLICCenterList);

__global__ void kUpdateClusterCentersImproved(const float4* __restrict__ floatBuffer, const int* __restrict__ maskBuffer,
										int nWidth, int nHeight, int clusterSize,
										SLICClusterCenter* vSLICCenterList);

void enforceConnectivityNative(int* maskBuffer,int width, int height, int nSeg);

void enforceConnectivityModified(int* maskBuffer,int width, int height, int &nSeg, int* pixelsOfSP);

#endif
