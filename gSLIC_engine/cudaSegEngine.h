#ifndef __CUDA_SEG_ENGINE__
#define __CUDA_SEG_ENGINE__

#include "cuda.h"
#include "cuda_runtime.h"
#include "device_launch_parameters.h"

extern "C" __host__ void InitCUDA(int width, int height,int nSegment, int nIterations);
extern "C" __host__ void CUDALoadImg(unsigned char* imgPixels);

extern "C" __host__ void TerminateCUDA();
extern "C" __host__ void CopyMaskDeviceToHost(int* maskPixels, int width, int height);
extern "C" __host__ void CudaSegmentation(int nSegments, int eSegmethod, double weight);
extern "C" __host__ void CopyMaskHostToDevice( int* maskPixels, int* pixelsOfSP, int width, int height);
extern "C" __host__ void CudaFeatures(int nSeg, int maxSPsize);
extern "C" __host__ void CopyMeanValuesDeviceToHost(float3* hMeanValues,int nSeg);
#endif