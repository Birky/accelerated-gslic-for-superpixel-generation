#include <time.h>
#include <stdio.h>
#include "cuda.h"
#include "cuda_runtime.h"
#include "device_launch_parameters.h"
#include "..\gSLIC_engine\FastImgSeg.h"
#include <opencv2/core/core.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/imgproc/imgproc.hpp>
#include "opencv\cv.h"
#include "opencv\highgui.h"
#include <fstream>
#include <string>

using namespace std;

extern int SPSV;
extern int CSCV;
extern int ECV;

/*********************************************************
*	Function handling the left mouse button click
*	coordinates	on the image.
*********************************************************/
void onMouse(int evt, int x, int y, int flags, void* param)
{
	if(evt == CV_EVENT_LBUTTONDOWN)
	{
		int *input = (int*)param;
		input[0] = x;
		input[1] = y;
	}
}

int main(int argc, char *argv[] )
{
	// Example input parameters
	// image 640 480 1200 10 5 1 0 C:\SegmentedImages\ 1 2 2 2 C:\images\2018.jpg C:\images\2019.jpg
	bool paCamera = true;
	string frameName;
	int paWidth = 640;
	int paHeight = 480;
	int paNumberOfSegments = 1200;
	float paCompactness = 10;
	int paIterations = 5; // 5 giving a good results
	bool paDisplay = 1; // 1 - true, 0 - false
	bool paSave = 0; // 1 - true, 0 - false
	string paSaveLocation = "C:\\";
	int paSegmentationColorSpace = 1; // 1 - Lab, 2 - XYZ, 3 - RGB
	int pacolorSpaceConversion = 2; // 1 - GPU-native, 2 - GPU-improved
	int paSuperpixelSegmentation = 2; // 1 - gSLIC-native, 2 - gSLIC-improved 
	int paEnforceConnectivity = 2; // 1 - CPU-native, 2 - CPU-modified, 3 - CPU-erode/dilate, 4 - none
	
	cudaSetDevice(0);
	
	if(argc > 13)
	{
		// use camera input or image/s
		if(strcmp(argv[1],"image") == 0)
		{
			paCamera = false;
		}

		// frame width and height
		paWidth = atoi(argv[2]);
		paHeight = atoi(argv[3]);

		paNumberOfSegments = atoi(argv[4]);
		paCompactness = atof(argv[5]);
		paIterations = atoi(argv[6]);
		paDisplay = atoi(argv[7]);
		paSave = atoi(argv[8]);
		paSaveLocation = argv[9]; 

		paSegmentationColorSpace = atoi(argv[10]);
		pacolorSpaceConversion = atoi(argv[11]);
		paSuperpixelSegmentation = atoi(argv[12]);
		paEnforceConnectivity = atoi(argv[13]);
	}
	
	CSCV = pacolorSpaceConversion;
	SPSV = paSuperpixelSegmentation;
	ECV = paEnforceConnectivity;

	CvCapture* capture = 0;
	IplImage* oldframe;

	if(paCamera) // camera input
	{
		capture = cvCaptureFromCAM(0);
		// if you want use video file as input:
        //capture = cvCaptureFromAVI("VIDEO_FILE_PATH");
		if (!capture)
		{		
			cout << "Camera couldn't be opened.";
			int c = cvWaitKey(5000);	
			cvReleaseCapture( &capture );
			return 1;
		}
	}
	

	IplImage* frame=cvCreateImage(cvSize(paWidth,paHeight),8,3);
	FastImgSeg* mySeg=new FastImgSeg();

	mySeg->initializeFastSeg(frame->width,frame->height,paNumberOfSegments, paIterations);
	
// creating an image buffer and inicializing it
	unsigned char* imgBuffer=(unsigned char*)malloc(frame->width*frame->height*sizeof(unsigned char)*4);
	memset(imgBuffer,0,frame->width*frame->height*sizeof(unsigned char)*4);

/*********************************************************
*	The segmentation loop
*	If the camera is the input source, it is a endless 
*	loop, which can be ended pressing the ESC key
*	If image/s is/are the input source, it loops over
*	all of the images and display them segmented
*********************************************************/
	for(int i = 14; i < argc || paCamera; i++)
	{
		string imageName;
		if(paCamera)
		{
			frameName = "Camera stream";
			oldframe=cvQueryFrame(capture);
		}else
		{
			string imageFullName(argv[i]);
			int pos = imageFullName.find_last_of("\\");
			imageName = imageFullName.substr(pos+1);
			frameName = "Image: " + imageName;
			oldframe=cvLoadImage(imageFullName.c_str(),1);
		}
		if(!oldframe) break;

		cvResize(oldframe,frame);

		// copy the frame to the image buffer
		for (int i=0;i<frame->height;i++)
		{
			for (int j=0;j<frame->width;j++)
			{
				int bufIdx=(i*frame->width+j)*4;
				imgBuffer[bufIdx]=CV_IMAGE_ELEM(frame,unsigned char,i,j*3);
				imgBuffer[bufIdx+1]=CV_IMAGE_ELEM(frame,unsigned char,i,j*3+1);
				imgBuffer[bufIdx+2]=CV_IMAGE_ELEM(frame,unsigned char,i,j*3+2);
			}
		}

		// copy the image to the GPU
		mySeg->LoadImg(imgBuffer); 

		// start the segmentation
		mySeg->DoSegmentation(paSegmentationColorSpace,paCompactness);

		// draw the boundaries of superpixels
		mySeg->Tool_GetMarkedImg(-1,-1); 

		for (int i=0;i<frame->height;i++)
		{
			for (int j=0;j<frame->width;j++)
			{
				int bufIdx=(i*frame->width+j)*4;

				CV_IMAGE_ELEM(frame,unsigned char,i,j*3)=mySeg->markedImg[bufIdx];
				CV_IMAGE_ELEM(frame,unsigned char,i,j*3+1)=mySeg->markedImg[bufIdx+1];
				CV_IMAGE_ELEM(frame,unsigned char,i,j*3+2)=mySeg->markedImg[bufIdx+2];
			}
		}
	
		if(paDisplay){
			// show the image/frame
			cvShowImage(frameName.c_str(),frame);
		}

		if(paSave)
		{
			// save the image/s
			string fullSaveLocation = paSaveLocation + imageName;
			cvSaveImage(fullSaveLocation.c_str(),frame);
		}

		if( paCamera && cvWaitKey(10) == 27 ) // End if ESC is pressed
			break;

		// mark the selected superpixel by mouse - ONLY in case of image/s
		int xy[2] = {-1,-1};
		while(!paCamera)
		{
			cvSetMouseCallback(frameName.c_str(),onMouse, (void*)&xy);
			if(paDisplay)
				cvShowImage(frameName.c_str(),frame);
			if(xy[0] != -1 && xy[1] != -1)
			{
				mySeg->Tool_GetMarkedImg(xy[0],xy[1]);

				for (int i=0;i<frame->height;i++)
				{
					for (int j=0;j<frame->width;j++)
					{
						int bufIdx=(i*frame->width+j)*4;

						CV_IMAGE_ELEM(frame,unsigned char,i,j*3)=mySeg->markedImg[bufIdx];
						CV_IMAGE_ELEM(frame,unsigned char,i,j*3+1)=mySeg->markedImg[bufIdx+1];
						CV_IMAGE_ELEM(frame,unsigned char,i,j*3+2)=mySeg->markedImg[bufIdx+2];
					}
				}
				xy[0] = -1;
				xy[1] = -1;
 			}
			if( cvWaitKey(10) == 27 ) // End if ESC is pressed
			{	
				if(paDisplay)
				cvDestroyAllWindows();
				break;
			}
		}
	}
	
	free(imgBuffer);
	if(paCamera)
	{
		cvReleaseCapture( &capture );
		if(paDisplay)
		cvDestroyWindow( frameName.c_str() );
	}
}